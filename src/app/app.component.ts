import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  name = 'Angular 5';
  lat: any;
  lng: any;
  constructor() {
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        this.lng = +pos.coords.longitude;
        this.lat = +pos.coords.latitude;
      });
    }

    console.log(navigator.appCodeName)
    console.log(navigator.appName)
    console.log(navigator.appVersion)
    console.log(navigator.language)
    console.log(navigator.platform)
  }
}
